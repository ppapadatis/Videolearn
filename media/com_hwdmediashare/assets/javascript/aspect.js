/**
 * @version    $Id: aspect.js 1112 2013-02-13 13:31:50Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) ${date?date?string("yyyy")} Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      ${date} ${time}
 */

/**
 * hwdms.aspect
 *
 * Function to vertically align thumbnails where the aspect has been forced
 */
window.addEvent('load', function () {
    var galleryImgs = $$('.media-thumb');
    var containers = $$('.items-row .media-item');
    var container = containers[0].getSize().y/2;

    if (galleryImgs.length > 0) galleryImgs.each(function(image) {	
        var margin = (container - (image.height/2));
        image.setStyle('margin-top', margin + 'px');	
    });     
});

