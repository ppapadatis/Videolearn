<?php
/**
 * @version    SVN $Id: fieldvalue.php 277 2012-03-28 10:03:31Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Field value table class
 */
class hwdMediaShareTableFieldValue extends JTable
{
	var $id 		= null;
	var $element_type	= null;
	var $field_id	        = null;
	var $value		= null;
	var $access		= null;

	public function __construct( &$db )
	{
		parent::__construct( '#__hwdms_fields_values', 'id', $db );
	}
        
	/**
	 * Overloaded load function
	 *
	 * @param       int $pk primary key
	 * @param       boolean $reset reset data
	 * @return      boolean
	 * @see JTable:load
	 */
	public function load( $elementType , $elementId, $fieldId )
	{
		$db		= $this->getDBO();
		$query	= 'SELECT * FROM ' . $db->nameQuote( '#__hwdms_fields_values' ) . ' '
				. 'WHERE ' . $db->nameQuote('field_id') . ' = ' . $db->Quote( $fieldId ) . ''
                                . 'AND ' . $db->nameQuote('element_type') . '=' . $db->Quote( $elementType ) . ''
                                . 'AND ' . $db->nameQuote('element_id') . '=' . $db->Quote( $elementId );
		$db->setQuery( $query );
		$result	= $db->loadObject();

		return $this->bind( $result );
	}
}
