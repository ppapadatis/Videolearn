<?php
/**
 * @version    SVN $Id: configuration.php 925 2013-01-16 11:16:38Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Import Joomla table library
jimport('joomla.database.table');

/**
 * Configuration table class
 */
class hwdMediaShareTableConfiguration extends JTable
{
	var $id                 = null;
	var $name		= null;
	var $date		= null;
        var $params		= null;
        var $asset_id		= null;

        /**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct( '#__hwdms_config' , 'id' , $db );
                
                // Here, we set the asset_id for the table. Since Joomla 2.5.7 there is new code 
                // to "Create an asset_id or heal one that is corrupted" which broke HWDMediaShare 
                // because the configuration table doesn't have an asset_id column. This is a work
                // around for that issue.                
		$name = $this->_getAssetName();
		$asset = JTable::getInstance('Asset', 'JTable', array('dbo' => $this->getDbo()));
		$asset->loadByName($name);
		$this->asset_id = $asset->id;
	}

	/**
	 * Overloaded bind function
	 *
	 * @param       array           named array
	 * @return      null|string     null is operation was satisfactory, otherwise returns an error
	 * @see         JTable:bind
	 * @since       0.1
	 */
	public function bind($array, $ignore = '')
        {
                // Bind the rules.
		if (isset($array['rules']) && is_array($array['rules']))
                {
                        // Unset empty (inherited) rules to avoid them being set to Denied
                        foreach( $array['rules'] as $action=>$identity )
                        {
                            foreach( $identity as $rule=>$value )
                            {
                                if( $value == "" ) unset( $array['rules'][$action][$rule] );
                            }
                        }

                        $this->setRules($array['rules']);
                        //@TODO: Remove this
                        $this->_trackAssets = true;
		}
		return parent::bind($array, $ignore);
	}

	/**
	 * Redefined asset name
	 */
        protected function _getAssetName()
        {
		return 'com_hwdmediashare';
        }
}
