<?php
/**
 * @version    SVN $Id: configuration.php 635 2012-10-19 13:06:56Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * hwdMediaShare Controller
 */
class hwdMediaShareControllerConfiguration extends JControllerForm
{
        var $view_list = "dashboard";
        
	/**
	 * Method to toggle the featured setting of a list of items.
	 *
	 * @return	void
	 * @since	0.1
	 */
	function background()
	{
                // Load hwdMediaShare config
                $hwdms = hwdMediaShareFactory::getInstance();
                $config = $hwdms->getConfig();
                $result = false;
                
                // Delete test file is already exists
                jimport( 'joomla.filesystem.file' );
                $filename = JPATH_SITE.'/tmp/hwdms.background';
		if (JFile::exists($filename)) JFile::delete($filename);

                $cli = JPATH_SITE.'/administrator/components/com_hwdmediashare/cli.php';

                // Try to create test file in background
		if(substr(PHP_OS, 0, 3) != "WIN") 
                {
			exec("env -i ".$config->get('path_php')." $cli test &>/dev/null &");
		} 
                else 
                {
			exec($config->get('path_php')." $cli test NUL");
		}
                
                // Sleep for 2 seconds
		usleep(1000000);
                
                // Check if file exists
                if (JFile::exists($filename)) 
                {
                        JFile::delete($filename);
                        $result = true;
                }  
                ?>
                <table class="adminlist">
                <tbody>        
                    <tr class="row0">
                        <td>
                            <?php echo JText::_('COM_HWDMS_TEST_RESULT'); ?>
                        </td>
                        <td>
                            <div class="fltrt">
                            <?php if ($result) : ?>
                                <div class="jgrid"><span class="state publish"><span class="text"><?php echo JText::_('COM_HWDMS_SUCCESS'); ?></span></span></div>
                            <?php else : ?>
                                <div class="jgrid"><span class="state unpublish"><span class="text"><?php echo JText::_('COM_HWDMS_FAIL'); ?></span></span></div>
                            <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                    <tr class="row1">
                        <td colspan="2">
                            <?php if ($result) : ?>
                                <h2><?php echo JText::_('COM_HWDMS_GOOD_NEWS'); ?></h2>
                                <p><?php echo JText::_('COM_HWDMS_AUTO_PROCESS_SUCCESS'); ?></p>
                            <?php else : ?>
                                <h2><?php echo JText::_('COM_HWDMS_BAD_NEWS'); ?></h2>
                                <p><?php echo JText::_('COM_HWDMS_AUTO_PROCESS_FAIL'); ?></p>
                            <?php endif; ?>
                        </td>
                    </tr> 
                </tbody>
                </table>
                <?php
	}
}
