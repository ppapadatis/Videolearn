<?php
/**
 * @version    SVN $Id: reported.php 425 2012-06-28 07:48:57Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2012 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      20-Feb-2012 20:19:14
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * hwdMediaShare Model
 */
class hwdMediaShareModelReported extends JModel
{
        /**
	 * Method to count media media.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getMedia($pk = null)
	{
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('1');
                $query->from('#__hwdms_reports AS a');
                $query->where('a.element_type = 1');
                $query->group('a.element_id');
                $db->setQuery($query);
                $db->query();                
                return $db->getNumRows();
	}
        
        /**
	 * Method to count media media.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getAlbums($pk = null)
	{
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('1');
                $query->from('#__hwdms_reports AS a');
                $query->where('a.element_type = 2');
                $query->group('a.element_id');
                $db->setQuery($query);
                $db->query();                
                return $db->getNumRows();
	}
        
        /**
	 * Method to count media media.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getGroups($pk = null)
	{
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('1');
                $query->from('#__hwdms_reports AS a');
                $query->where('a.element_type = 3');
                $query->group('a.element_id');
                $db->setQuery($query);
                $db->query();                
                return $db->getNumRows();
	}
        
        /**
	 * Method to count media media.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getUsers($pk = null)
	{
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('1');
                $query->from('#__hwdms_reports AS a');
                $query->where('a.element_type = 5');
                $query->group('a.element_id');
                $db->setQuery($query);
                $db->query();                
                return $db->getNumRows();
	}  
        
        /**
	 * Method to count media media.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getPlaylists($pk = null)
	{
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('1');
                $query->from('#__hwdms_reports AS a');
                $query->where('a.element_type = 4');
                $query->group('a.element_id');
                $db->setQuery($query);
                $db->query();                
                return $db->getNumRows();
	}
        
        /**
	 * Method to count media media.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getActivities($pk = null)
	{
                $db =& JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->select('1');
                $query->from('#__hwdms_reports AS a');
                $query->where('a.element_type = 7');
                $query->group('a.element_id');
                $db->setQuery($query);
                $db->query();                
                return $db->getNumRows();
	}
}
