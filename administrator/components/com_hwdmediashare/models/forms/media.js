/**
 * @version    SVN $Id: media.js 277 2012-03-28 10:03:31Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

window.addEvent('domready', function() {
	document.formvalidator.setHandler('integer',
		function (value) {
			regex=/^[0-9]+$/;
			return regex.test(value);
	});
});