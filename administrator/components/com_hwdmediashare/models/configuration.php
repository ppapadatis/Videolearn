<?php
/**
 * @version    SVN $Id: configuration.php 921 2013-01-16 11:09:27Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * hwdMediaShare Model
 */
class hwdMediaShareModelConfiguration extends JModelAdmin
{
        /**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	0.1
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Check specific edit permission then general edit permission.
		return JFactory::getUser()->authorise('core.edit', 'com_hwdmediashare.configuration.'.((int) isset($data[$key]) ? $data[$key] : 0)) or parent::allowEdit($data, $key);
	}
        /**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	0.1
	 */
	public function getTable($type = 'Configuration', $prefix = 'hwdMediaShareTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		Data for the form.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	mixed	A JForm object on success, false on failure
	 * @since	0.1
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_hwdmediashare.configuration', 'configuration', array('control' => 'jform', 'load_data' => $loadData));
                if (empty($form))
		{
			return false;
		}
		return $form;
	}
        /**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	Script files
	 */
	public function getScript()
	{
		return 'administrator/components/com_hwdmediashare/models/forms/configuration.js';
	}
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	0.1
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_hwdmediashare.edit.congiguration.data', array());
		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}
        /**
	 * Method to save the configuration data.
	 *
	 * @param	array	An array containing all global config data.
	 * @return	bool	True on success, false on failure.
	 * @since	0.1
	 */
	public function save($data)
	{
 		// Initialise variables.
                $data = JRequest::getVar('jform', array(), 'post', 'array');
                $date =& JFactory::getDate();

                jimport( 'joomla.filesystem.file');
                $ini	= JPATH_ROOT.'/administrator/components/com_hwdmediashare/config.ini';
                $defaultConfig = JFile::read($ini);

                // Load default configuration
                $config	= new JRegistry( $defaultConfig );

                $dataWithoutRules = $data;
                unset($dataWithoutRules['rules']);

                // Bind the user saved configuration.
                $config->loadArray($dataWithoutRules);

                $configObject = JRegistryFormatJSON::stringToObject($config);
                $configJson = JRegistryFormatJSON::objectToString($configObject);

                $array['id'] = 1;
                $array['name'] = 'config';
                $array['date'] = $date->format('Y-m-d H:i:s');
                $array['params'] = (string)$configJson;
                $array['rules'] = $data['rules'];
                        
                if (parent::save($array))
                {
                        return true;
                }   

		return false;
	}
}
