<?php
/**
 * @version    $Id: default.php 425 2012-06-28 07:48:57Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
?>
<ul class="categories-module<?php echo $moduleclass_sfx; ?>">
<?php require JModuleHelper::getLayoutPath('mod_media_categories', $params->get('layout', 'default').'_items'); ?>
</ul>