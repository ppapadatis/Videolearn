<?php
/**
 * @version    SVN $Id: media.php 1100 2013-02-12 13:08:20Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2012 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      27-Feb-2012 16:29:22
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Avoid problems with RokComment plugin #55621
JLoader::register('ContentModelCategories', JPATH_ROOT.'/components/com_content/models/categories.php');

class plgContentMedia extends JPlugin
{
	/**
	 * An item.
	 *
	 * @var    array
	 * @since  11.1
	 */
	protected $item = null;

        /**
	 * Plugin that loads module positions within content
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The article object.  Note $article->text is also available
	 * @param	object	The article params
	 * @param	int	The 'page' number
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		$app = JFactory::getApplication();
                
                // Simple admin check to determine whether plugin should process further
		if ($app->isAdmin()) return true;
                
                // Simple performance check to determine whether plugin should process further
		if (strpos($article->text, 'media') === false) {
			return true;
		}
                
                JTable::addIncludePath(JPATH_ROOT . '/administrator/components/com_hwdmediashare/tables');
                JLoader::register('hwdMediaShareFactory', JPATH_ROOT . '/components/com_hwdmediashare/libraries/factory.php');
                $hwdms = hwdMediaShareFactory::getInstance();
                $this->config = $hwdms->getConfig();
                $this->config->merge( $this->params );
                
		// Expression to search for (positions)
		$regex	= '/{media\s+(.*?)}/i';

		// Find all instances of plugin and put in $matches for loadposition
		// $matches[0] is full pattern match, $matches[1] is the position
		preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);

                // No matches, skip this
		if ($matches)
                {
			foreach ($matches as $match)
                        {
                                $matcheslist =  explode(',', $match[1]);

                                $options = array();
                                $data = '';

                                foreach ($matcheslist as $list)
                                {
                                        $data.= "$list\n";
                                }


                                // Load default configuration
                                jimport( 'joomla.html.parameter' );
                                $this->config->merge( new JRegistry( $data ) );

                                // Here we take the more human readable width option and set the 
                                // main mediaitem_size parameter
                                $this->config->set('mediaitem_size', $this->config->get('width', 200));
                                $this->config->set('show_more_link', 'hide');
                                
				$align = (($this->config->get('align', 'left') == 'center') ? 'margin: 0 auto;' : 'float:'.$this->config->get('align', 'left').';');

                                $output = '<div class="media-content" style="width:'.$this->config->get('width', 200).'px;'.$align.'">';
                                $output.= $this->_load();
                                $output.= '</div>';

                                // We should replace only first occurrence in order to allow positions with the same name to regenerate their content:
				$article->text = preg_replace("|$match[0]|", addcslashes($output, '\\'), $article->text, 1);
			}
		}
	}

	protected function _load()
	{
                if (!$this->config->def('id'))
                {
                        return '<!-- Failed media plugin: ' . $this->config . '-->';
                }
                if (!$this->config->def('load'))
                {
                        return '<!-- Failed media plugin: ' . $this->config . '-->';
                }

                JLoader::register('hwdMediaShareHelperNavigation', JPATH_ROOT . '/components/com_hwdmediashare/helpers/navigation.php');
                JLoader::register('hwdMediaShareModelMediaItem', JPATH_ROOT . '/components/com_hwdmediashare/models/mediaitem.php');

                // Load the HWDMediaShare language file
                $lang =& JFactory::getLanguage();
                $lang->load('com_hwdmediashare', JPATH_SITE.'/components/com_hwdmediashare', $lang->getTag());
                
                $model = hwdMediaShareModelMediaItem::getInstance('MediaItem', 'hwdMediaShareModel');
                $item = $model->getItem($this->config->get('id'));

                if (empty($item))
                {
                        return '<!-- Failed media plugin: ' . $this->config . '-->';
                }

                hwdMediaShareFactory::load('downloads');
                hwdMediaShareFactory::load('files');
                hwdMediaShareFactory::load('utilities');

                JLoader::register('hwdMediaShareHelperRoute', JPATH_ROOT . '/components/com_hwdmediashare/helpers/route.php');
                JLoader::register('JHtmlString', JPATH_LIBRARIES.'/joomla/html/html/string.php');
                JLoader::register('JHtmlHwdIcon', JPATH_ROOT.'/components/com_hwdmediashare/helpers/icon.php');


                $params	= $this->config;
                
                $helper	= new JObject;
                $helper->set('utilities', hwdMediaShareUtilities::getInstance());
                $helper->set('return', base64_encode(JFactory::getURI()->toString()));
                $helper->set('columns', 1);

                $items = array($item);
                
                $lang =& JFactory::getLanguage();
                $lang->load('com_hwdmediashare');

		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JURI::base( true ).'/media/com_hwdmediashare/assets/css/lite.css');

                ob_start();
                if ($this->config->get('display') == 'inline')
                {
                        if (file_exists(JPATH_SITE . '/modules/mod_media_item/helper.php'))
                        {
                                require JModuleHelper::getLayoutPath('mod_media_item', $params->get('layout', 'default'));
                        }
                }
                else
                {
                        if (file_exists(JPATH_SITE . '/modules/mod_media_media/helper.php'))
                        {
                                // Check if we should set the modal parameter
                                if ($this->config->get('display') == 'modal') $params->set('modal', 1);

                                // Load module layout
                                JLoader::register('modMediaMediaHelper', JPATH_SITE . '/modules/mod_media_media/helper.php');
                                $dummy = new JObject;
                                $dummy->id = 0;
                                $helper	= new modMediaMediaHelper($dummy, $params);
                                require JModuleHelper::getLayoutPath('mod_media_media', $params->get('layout', 'default'));
                        }                    

                }
                $retval = ob_get_contents();
                ob_end_clean();

		return $retval;
	}
}
