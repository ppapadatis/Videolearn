<?php
/**
 * @version    SVN $Id: navigation.php 496 2012-08-29 13:26:32Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      14-Nov-2011 20:36:41
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * hwdMediaShare Navigation Helper
 *
 * @package	hwdMediaShare
 * @since       0.1
 */
abstract class hwdMediaShareHelperNavigation
{
        /**
	 * Method to insert Javascript declaration for live site variable,
	 *
	 * @since	0.1
	 */
	public static function setJavascriptVars()
	{
                $doc = & JFactory::getDocument();
                $js = array();
                $js[] = 'var hwdms_live_site = "' . JURI::root() . 'index.php";';
                $doc->addScriptDeclaration( implode("\n", $js) );
        }    
        
        /**
	 * Method to insert internal navigation using hwdMediaShare Joomla menu,
         * or fallback static menu
	 *
	 * @since	0.1
	 */
	public static function getInternalNavigation()
	{
                $app	= JFactory::getApplication();
                $view   = JRequest::getWord('view');
                $html   = '';
                $tmpl   = JRequest::getWord( 'tmpl', '' );
                
                JHtml::_('behavior.modal');

                // Load hwdMediaShare config
                $hwdms = hwdMediaShareFactory::getInstance();
                $config = $hwdms->getConfig();

                if ($config->get('internal_navigation') != 0 && $tmpl != 'component')
                {               
                        JLoader::register('modMenuHelper', JPATH_SITE . '/modules/mod_menu/helper.php');

                        $params	= new JRegistry( '{"menutype":"hwdmediashare","show_title":""}' );

                        $list           = modMenuHelper::getList($params);
                        $app            = JFactory::getApplication();
                        $menu           = $app->getMenu();
                        $active         = $menu->getActive();
                        $active_id      = isset($active) ? $active->id : $menu->getDefault()->id;
                        $path           = isset($active) ? $active->tree : array();
                        $showAll	= $params->get('showAllChildren');
                        $class_sfx	= htmlspecialchars($params->get('class_sfx'));

                        ob_start();
                        ?>
                        <!-- Media Main Navigation -->
                        <div class="media-mediamenu">
                            <?php
                            if(count($list)) 
                            {
                                    require JModuleHelper::getLayoutPath('mod_menu', $params->get('layout', 'default'));
                            }
                            else
                            {
                            ?>
                                <ul class="menu">
                                    <li class="<?php echo ($view == 'discover' ? 'active ' : false); ?>media-medianav-discover"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=discover'); ?>"><?php echo JText::_('COM_HWDMS_DISCOVER'); ?></a></li>
                                    <li class="<?php echo (($view == 'media' || $view == 'mediaitem') ? 'active ' : false); ?>media-medianav-media"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=media'); ?>"><?php echo JText::_('COM_HWDMS_MEDIA'); ?></a></li>
                                    <li class="<?php echo (($view == 'categories' || $view == 'category') ? 'active ' : false); ?>media-medianav-categories"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=categories'); ?>"><?php echo JText::_('COM_HWDMS_CATEGORIES'); ?></a></li>
                                    <li class="<?php echo (($view == 'albums' || $view == 'album') ? 'active ' : false); ?>media-medianav-albums"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=albums'); ?>"><?php echo JText::_('COM_HWDMS_ALBUMS'); ?></a></li>
                                    <li class="<?php echo (($view == 'groups' || $view == 'group') ? 'active ' : false); ?>media-medianav-groups"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=groups'); ?>"><?php echo JText::_('COM_HWDMS_GROUPS'); ?></a></li>
                                    <li class="<?php echo (($view == 'playlists' || $view == 'playlist') ? 'active ' : false); ?>media-medianav-playlists"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=playlists'); ?>"><?php echo JText::_('COM_HWDMS_PLAYLISTS'); ?></a></li>
                                    <li class="<?php echo (($view == 'users' || $view == 'user') ? 'active ' : false); ?>media-medianav-channels"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=users'); ?>"><?php echo JText::_('COM_HWDMS_USER_CHANNELS'); ?></a></li>
                                    <li class="<?php echo ($view == 'upload' ? 'active ' : false); ?>media-medianav-upload"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=upload'); ?>"><?php echo JText::_('COM_HWDMS_UPLOAD'); ?></a></li>
                                    <li class="<?php echo ($view == 'account' ? 'active ' : false); ?>media-medianav-account"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account'); ?>"><?php echo JText::_('COM_HWDMS_MY_ACCOUNT'); ?></a></li>
                                    <li class="<?php echo ($view == 'search' ? 'active ' : false); ?>media-medianav-search"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=search'); ?>"><?php echo JText::_('COM_HWDMS_SEARCH'); ?></a></li>
                                </ul>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        $html = ob_get_contents();
                        ob_end_clean();
                }
		return $html;
	}
	/**
	 * Method to insert accoutn navigation menu,
	 *
	 * @since	0.1
	 */
	public static function getAccountNavigation()
	{
                $user = JFactory::getUser();
                $uri	= JFactory::getURI();
                
                // Load hwdMediaShare config
                $hwdms = hwdMediaShareFactory::getInstance();
                $config = $hwdms->getConfig();

                ob_start();
                ?>                     
                <!-- Media Main Navigation -->
                <div class="media-accountmenu">
                  <ul class="media-accountnav">
                    <li class="media-accountnav-overview"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account'); ?>"><?php echo JText::_('COM_HWDMS_OVERVIEW'); ?></a></li>
                    <li class="media-accountnav-profile"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&task=userform.edit&id='.$user->id.'&return='.base64_encode($uri)); ?>"><?php echo JText::_('COM_HWDMS_PROFILE'); ?></a></li>
                    <li class="media-accountnav-media"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account&layout=media'); ?>"><?php echo JText::_('COM_HWDMS_MY_MEDIA'); ?></a></li>
                    <li class="media-accountnav-favourites"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account&layout=favourites'); ?>"><?php echo JText::_('COM_HWDMS_MY_FAVOURITES'); ?></a></li>
                    <?php if ($config->get('enable_albums')): ?><li class="media-accountnav-albums"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account&layout=albums'); ?>"><?php echo JText::_('COM_HWDMS_MY_ALBUMS'); ?></a></li><?php endif; ?>
                    <?php if ($config->get('enable_groups')): ?><li class="media-accountnav-groups"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account&layout=groups'); ?>"><?php echo JText::_('COM_HWDMS_MY_GROUPS'); ?></a></li><?php endif; ?>
                    <?php if ($config->get('enable_playlists')): ?><li class="media-accountnav-playlists"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account&layout=playlists'); ?>"><?php echo JText::_('COM_HWDMS_MY_PLAYLISTS'); ?></a></li><?php endif; ?>
                    <?php if ($config->get('enable_subscriptions')): ?><li class="media-accountnav-subscriptions"><a href="<?php echo JRoute::_('index.php?option=com_hwdmediashare&view=account&layout=subscriptions'); ?>"><?php echo JText::_('COM_HWDMS_MY_SUBSCRIPTIONS'); ?></a></li><?php endif; ?>
                  </ul>
                </div>
                <?php
                $html = ob_get_contents();
                ob_end_clean();

		return $html;
	}
        
        /**
	 * @since	0.1
	 */
	public function pageNavigation(&$row, &$params, $page=0)
	{
		$view = JRequest::getCmd('view');
		$print = JRequest::getBool('print');

                // Get additional filters
		$album_id = JRequest::getCmd('album_id');

		if ($print)
                {
			return false;
		}

                $nav = new StdClass;

		//if ($params->get('show_item_navigation') && ($view == 'article')) {
                if ($view == 'mediaitem')
                {
			$html = '';
			$db	= JFactory::getDbo();
			$user	= JFactory::getUser();
			$app	= JFactory::getApplication();
			$lang	= JFactory::getLanguage();
			$nullDate = $db->getNullDate();

			$date	= JFactory::getDate();
			$config	= JFactory::getConfig();
			$now	= $date->toSql();

			$uid	= $row->id;
			$canPublish = $user->authorise('core.edit.state', 'com_hwdmediashare.media.'.$row->id);

			// The following is needed as different menu items types utilise a different param to control ordering.
			// For Blogs the `orderby_sec` param is the order controlling param.
			// For Table and List views it is the `orderby` param.
			$params_list = $params->toArray();
			if (array_key_exists('orderby_sec', $params_list)) {
				$order_method = $params->get('orderby_sec', '');
			} else {
				$order_method = $params->get('orderby', '');
			}
                        
			// Additional check for invalid sort ordering.
			if ($order_method == 'front') {
				$order_method = '';
			}

			// Determine sort order.
			switch ($order_method) {
				case 'date' :
					$orderby = 'a.created';
					break;
				case 'rdate' :
					$orderby = 'a.created DESC';
					break;
				case 'alpha' :
					$orderby = 'a.title';
					break;
				case 'ralpha' :
					$orderby = 'a.title DESC';
					break;
				case 'hits' :
					$orderby = 'a.hits';
					break;
				case 'rhits' :
					$orderby = 'a.hits DESC';
					break;
				case 'order' :
					$orderby = 'a.ordering';
					break;
				case 'author' :
					$orderby = 'a.created_by_alias, u.name';
					break;
				case 'rauthor' :
					$orderby = 'a.created_by_alias DESC, u.name DESC';
					break;
				case 'front' :
					$orderby = 'f.ordering';
					break;
				default :
					$orderby = 'a.ordering';
					break;
			}

			$xwhere = ' AND (a.published = 1 OR a.published = -1)' .
			' AND (publish_up = '.$db->Quote($nullDate).' OR publish_up <= '.$db->Quote($now).')' .
			' AND (publish_down = '.$db->Quote($nullDate).' OR publish_down >= '.$db->Quote($now).')' .
			' AND (a.private = 0 OR (a.private = 1 && a.created_user_id = '.$user->id.'))';

			// Array of media (in same category) correctly ordered.
			$query	= $db->getQuery(true);
			$query->select('a.id, a.title, a.key, a.ext_id, a.thumbnail_ext_id, a.type, a.thumbnail');
			$query->from('#__hwdms_media AS a');
                        
			//$query->leftJoin('#__categories AS cc ON cc.id = a.catid');
			//$query->where('a.catid = '. (int)$row->catid .' AND a.state = '. (int)$row->state
			//			. ($canPublish ? '' : ' AND a.access = ' .(int)$row->access) . $xwhere);
                        
                        if ($album_id)
                        {
                                $query->leftJoin('#__hwdms_album_map AS amap ON amap.media_id = a.id');
                                $query->where('amap.album_id = '. (int)$album_id);                                
                        }

                        $query->where('(a.published = 1 OR a.published = -1)');
                        $query->where('(publish_up = '.$db->Quote($nullDate).' OR publish_up <= '.$db->Quote($now).')');
                        $query->where('(publish_down = '.$db->Quote($nullDate).' OR publish_down >= '.$db->Quote($now).')');
                        $query->where('(a.private = 0 OR (a.private = 1 && a.created_user_id = '.$user->id.'))');
                        
			$query->order($orderby);
                        
			if ($app->isSite() && $app->getLanguageFilter()) 
                        {
				$query->where('a.language in ('.$db->quote($lang->getTag()).','.$db->quote('*').')');
			}

			$db->setQuery($query);
			$list = $db->loadObjectList('id');

			// This check needed if incorrect Itemid is given resulting in an incorrect result.
			if (!is_array($list))
                        {
				$list = array();
			}

			reset($list);

			// Location of current content item in array list.
			$location = array_search($uid, array_keys($list));

			$navs = array_values($list);

                        $nav->prev = null;
			$nav->next = null;

			if ($location -1 >= 0)	
                        {
				// The previous content item cannot be in the array position -1.
				$nav->prev = $navs[$location -1];
			}

			if (($location +1) < count($navs)) 
                        {
				// The next content item cannot be in an array position greater than the number of array postions.
				$nav->next = $navs[$location +1];
			}
		}

		return $nav;
	}
}
