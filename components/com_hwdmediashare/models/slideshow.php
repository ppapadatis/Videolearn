<?php
/**
 * @version    SVN $Id: slideshow.php 561 2012-10-11 12:28:30Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      25-Oct-2011 13:00:18
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Base this model on the backend version.
require_once JPATH_SITE.'/components/com_hwdmediashare/models/mediaitem.php';

// Import Joomla modelitem library
jimport('joomla.application.component.modelform');

/**
 * hwdMediaShare Model
 */
class hwdMediaShareModelSlideshow extends hwdMediaShareModelMediaItem
{
	/**
	 * The active id.
	 *
	 * @var    array
	 */
	protected $_key = null;
        
	/**
	 * The slideshow items.
	 *
	 * @var    array
	 */
	protected $_items = null;

        /**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	0.1
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'created', 'a.created',
				'hits', 'a.hits',
				'title', 'a.title',
				'likes', 'a.likes',
				'dislikes', 'a.dislikes',
				'modified', 'a.modified',
				'viewed', 'a.viewed',
				'title', 'a.title',
                                'author', 'author',
                                'created', 'a.created',
                                'ordering', 'a.ordering',
                                'random', 'random',
			);
		}

		parent::__construct($config);
	}


        
        /**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 */
	public function getItems($pk = null)
	{
                JModel::addIncludePath(JPATH_ROOT.'/components/com_hwdmediashare/models');
                $model =& JModel::getInstance('Media', 'hwdMediaShareModel', array('ignore_request' => true));
                
		// Set application parameters in model
		$app = JFactory::getApplication();
		$appParams = $app->getParams();
		$model->setState('params', $appParams);

		// Set the filters based on the module params
		$model->setState('list.start', 0);
		$model->setState('list.limit', (int) 100);
                
		$user = JFactory::getUser();
		if ((!$user->authorise('core.edit.state', 'com_hwdmediashare')) &&  (!$user->authorise('core.edit', 'com_hwdmediashare')))
                {
			// Limit to published for people who can't edit or edit.state.
			$model->setState('filter.published',	1);
			$model->setState('filter.status',	1);

			// Filter by start and end dates.
			$model->setState('filter.publish_date', true);
		}
                else
                {
			// Limit to published for people who can't edit or edit.state.
			$model->setState('filter.published',	array(0,1));
			$model->setState('filter.status',	1);
                }
                
		// Ordering
		$model->setState('com_hwdmediashare.media.list.ordering', 'a.ordering');
		$model->setState('com_hwdmediashare.media.list.direction', 'ASC');

                if ($playlist_id = JRequest::getInt('playlist_id'))
                {
                        $model->setState('filter.playlist_id', JRequest::getInt('playlist_id'));
                        $model->setState('com_hwdmediashare.media.list.ordering', 'pmap.ordering');
                }        
                
		// Filter by language
		$model->setState('filter.language', $app->getLanguageFilter());

                if ($items = $model->getItems())
                {
                        for ($i=0, $n=count($items); $i < $n; $i++)
                        {
                                $this->_key = $items[$i]->id;                             
                                break;
                        }
                }

		$this->_items = $items; 
		return $items; 
	}
        
	/**
	 * Method to get media data.
	 *
	 * @param	integer	The id of the article.
	 *
	 * @return	mixed	Menu item data object on success, false on failure.
	 */
	public function &getItem($id = null)
	{
                // Load the parameters.
                $hwdms = hwdMediaShareFactory::getInstance();
                $config = $hwdms->getConfig();
                
		// Load the object state, and if missing load from the loaded items instead.
		$id	= JRequest::getInt('id', $this->_key);
                
                // If we have no available state then we can't view the slideshow
                if (empty($id)) 
                { 
                    JFactory::getApplication()->enqueueMessage( JText::_( 'COM_HWDMS_ERROR_NOAUTHORISED_ITEM' ) ); 
                    JFactory::getApplication()->redirect( $config->get('no_access_redirect') > 0 ? ContentHelperRoute::getArticleRoute($config->get('no_access_redirect')) : JRoute::_(base64_decode(JRequest::getVar('return', hwdMediaShareHelperRoute::getMediaItemRoute($this->item->id)))) );
                }
                
                // Update the media.id state
		$this->setState('media.id', $id);               

		// Attempt to load the row.
                return parent::getItem($id);
	}
}