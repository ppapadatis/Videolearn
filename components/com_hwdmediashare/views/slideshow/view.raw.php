<?php
/**
 * @version    SVN $Id: view.raw.php 496 2012-08-29 13:26:32Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Import Joomla view library
jimport('joomla.application.component.view');

/**
 * HTML View class for the hwdMediaShare Component
 */
class hwdMediaShareViewSlideshow extends JView
{
	// Overwriting JView display method
	function display($tpl = null)
	{
                $app = & JFactory::getApplication();

                // Get the Data
                $state = $this->get('State');
                $items = $this->get('Items');
                $key = $this->get('Key');

                // Download links
                hwdMediaShareFactory::load('files');
                hwdMediaShareFactory::load('downloads');
                hwdMediaShareFactory::load('media');
                hwdMediaShareFactory::load('utilities');
                JLoader::register('JHtmlHwdIcon', JPATH_COMPONENT . '/helpers/icon.php');

                // Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		$params = &$state->params;

                $this->assign('key',    		$key);

                $this->assignRef('params',		$params);
                $this->assignRef('items',		$items);
                $this->assignRef('utilities',		hwdMediaShareUtilities::getInstance());

                // Display the view
                parent::display($tpl);
	}
}
