<?php
/**
 * @version    SVN $Id: view.feed.php 425 2012-06-28 07:48:57Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2012 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      06-Jan-2012 15:40:58
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('joomla.document.feed.feed');

/**
 * HTML View class for the Content component
 *
 * @package		Joomla.Site
 * @subpackage	com_content
 * @since 1.5
 */
class hwdMediaShareViewMedia extends JView
{
	function display()
	{
                $app = JFactory::getApplication();
		$doc	= JFactory::getDocument();
                
                hwdMediaShareFactory::load('files');
                hwdMediaShareFactory::load('downloads');
                hwdMediaShareFactory::load('renderer.xml');
                hwdMediaShareFactory::load('renderer.rssgeo');
                hwdMediaShareFactory::load('renderer.xspf');

                // Initialise variables
		$state		= $this->get('State');
		$rows		= $this->get('Items');

		foreach ($rows as $row)
		{
			// strip html from feed item title
			$title = $this->escape($row->title);
			$title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');

			// Compute the article slug
			$row->slug = $row->alias ? ($row->id . ':' . $row->alias) : $row->id;
                        $description = '<img src="'.JRoute::_(hwdMediaShareDownloads::thumbnail($row)).'" border="0" alt="'.$this->escape($row->title).'" height="100" />';
			
                        // load individual item creator class
			$item = new JFeedItem();
			$item->title		= $title;
			$item->link		= JRoute::_(hwdMediaShareHelperRoute::getMediaItemRoute($row->slug));
			$item->description	= $description;
			$item->date		= ($row->created ? date('r', strtotime($row->created)) : '');
			$item->author		= $row->created_by_alias ? $row->created_by_alias : $row->author;
			$item->location		= $row->location;
                
                        // @TODO: Cleanup
                        hwdMediaShareFactory::load('videos');
                        if ($mp4 = hwdMediaShareVideos::getMp4($row))
                        {
                                $item->enclosure->url = $mp4;
                                $item->enclosure->length = "";
                                $item->enclosure->type = "video/mp4";
                        }
                        else if ($flv = hwdMediaShareVideos::getFlv($row))
                        {
                                $item->enclosure->url = $flv;
                                $item->enclosure->length = "";
                                $item->enclosure->type = "video/flv";
                        }
                        else
                        {
                                hwdMediaShareFiles::getLocalStoragePath();

                                $folders = hwdMediaShareFiles::getFolders($row->key);
                                $filename = hwdMediaShareFiles::getFilename($row->key, 1);
                                $ext = hwdMediaShareFiles::getExtension($row, 1);

                                $path = hwdMediaShareFiles::getPath($folders, $filename, $ext);
                                if (file_exists($path))
                                {
                                        $item->enclosure->url = hwdMediaShareFiles::getUrl($folders, $filename, $ext);
                                        $item->enclosure->length = "";
                                        $item->enclosure->type = "";
                                }
                        }
                        
			// loads item info into rss array
			$doc->addItem($item);
		}
	}
}
