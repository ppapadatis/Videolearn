<?php
/**
 * @version    SVN $Id: view.xspf.php 425 2012-06-28 07:48:57Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2012 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      06-Jan-2012 15:44:51
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
jimport('joomla.document.feed.feed');

/**
 * HTML View class for the Content component
 *
 * @package		Joomla.Site
 * @subpackage	com_content
 * @since 1.5
 */
class hwdMediaShareViewMedia extends JView
{
	function display()
	{
                $app = JFactory::getApplication();
		$doc	= JFactory::getDocument();
                $doc->setMimeEncoding( 'application/xhtml+xml' );
                
                // Initialise variables
		$state		= $this->get('State');
		$rows		= $this->get('Items');

                // Prepare output
		$html = array();
                $html[] = '<playlist version="1" xmlns="http://xspf.org/ns/0/">';
		$html[] = '<title>Example XSPF playlist</title>';
		$html[] = '<tracklist>';   
    
		foreach ($rows as $row)
		{
			// strip html from feed item title
			$title = $this->escape($row->title);
			$title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');

			// Compute the article slug
			$row->slug = $row->alias ? ($row->id . ':' . $row->alias) : $row->id;

			// url link to article
			$link = "";

			// strip html from feed item description text
			// TODO: Only pull fulltext if necessary (actually, just get the necessary fields).
			$description	= $row->description;
			//$author			= $row->created_by_alias ? $row->created_by_alias : $row->author;
			@$date			= ($row->created ? date('r', strtotime($row->created)) : '');

			$item->title		= $title;
			$item->link			= $link;
			$item->description	= $description;
			$item->date			= $date;
			//$item->category		= $row->category;
			//$item->author		= $author;

			// loads item info into rss array
                        $html[] = '<track>';
                        $html[] = '<title>Youtube video with start</title>';
                        $html[] = '<creator>the Peach Open Movie Project</creator>';
                        $html[] = '<info>http://www.bigbuckbunny.org/</info>';
                        $html[] = '<annotation>Big Buck Bunny is a short animated film by the Blender Institute, part of the Blender Foundation.</annotation>';
                        $html[] = '<location>http://youtube.com/watch?v=IBTE-RoMsvw</location>';
                        $html[] = '<meta rel="start">10</meta>';
                        $html[] = '</track>';
		}

                $html[] = '</tracklist>';
		$html[] = '</playlist>';
		
                print implode("\n", $html);
                return;
	}
}
