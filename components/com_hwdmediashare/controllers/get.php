<?php
/**
 * @version    SVN $Id: get.php 430 2012-07-11 09:08:36Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      25-May-2011 16:53:20
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * hwdMediaShare Controller
 */
class hwdMediaShareControllerGet extends JControllerForm
{       
	/**
	 * Method to render the display html of a media item
	 * @since	0.1
	 */
        function embed()
        {
                // Load hwdMediaShare config
                $hwdms = hwdMediaShareFactory::getInstance();
                $config = $hwdms->getConfig();
                $document = JFactory::getDocument();
                
                $config->set('mediaitem_size', JRequest::getInt('width', 560));

                $document->addStyleDeclaration('#main, .contentpane {
  margin: 0!important;
  padding: 0!important;
}');
                
                JRequest::setVar('tmpl','component');
                $id = JRequest::getInt( 'id' , '' );
                if ($id > 0)
                {
                        // Load group
                        JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_hwdmediashare/tables');
                        $table =& JTable::getInstance('Media', 'hwdMediaShareTable');
                        $table->load( $id );

                        $properties = $table->getProperties(1);
                        $row = JArrayHelper::toObject($properties, 'JObject');
                }
                else
                {
			return;
                }
                
                hwdMediaShareFactory::load('media');
                hwdMediaShareFactory::load('downloads');

                $row->media_type = hwdMediaShareMedia::loadMediaType($row);

                ob_start();
                ?>
                <div style="width:<?php echo JRequest::getInt('width', 560); ?>px;height:<?php echo JRequest::getInt('height', 315); ?>px;overflow:hidden;">
                <?php echo hwdMediaShareMedia::get($row); ?>
                </div>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
                print $html; 
        }
}
