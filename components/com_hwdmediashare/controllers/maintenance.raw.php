<?php
/**
 * @version    SVN $Id: maintenance.raw.php 388 2012-05-23 12:22:12Z dhorsfall $
 * @package    hwdMediaShare
 * @copyright  Copyright (C) 2011 Highwood Design Limited. All rights reserved.
 * @license    GNU General Public License http://www.gnu.org/copyleft/gpl.html
 * @author     Dave Horsfall
 * @since      15-Apr-2011 10:13:15
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class HwdMediaShareControllerMaintenance extends JController
{
        /**
	 * Method to run the mainenance
	 * @since	0.1
	 */
        function process()
        {
                $app =& JFactory::getApplication();

                // Check token
                if ($app->getCfg('secret') != JRequest::getVar('token'))
                {
                    die('Invalid secret token');
                }

                // Require hwdMediaShare factory
                JLoader::register('hwdMediaShareFactory', JPATH_BASE.'/components/com_hwdmediashare/libraries/factory.php');

                // Load process object
                hwdMediaShareFactory::load('processes');
                $model = hwdMediaShareProcesses::getInstance();                

                for ($i = 1; $i <= 50; $i++)
                {
                        $model->run();
                }
        }

        function cdn()
        {
                $app =& JFactory::getApplication();

                // Check token
                if ($app->getCfg('secret') != JRequest::getVar('token'))
                {
                    die('Invalid secret token');
                }
                
                // Require hwdMediaShare factory
                JLoader::register('hwdMediaShareFactory', JPATH_BASE.'/components/com_hwdmediashare/libraries/factory.php');

                JLoader::register('plgHwdmediashareCdn_amazons3', JPATH_BASE.'/plugins/hwdmediashare/cdn_amazons3/cdn_amazons3.php');
                $cdn = plgHwdmediashareCdn_amazons3::getInstance();
                $cdn->maintenance();
        }
}